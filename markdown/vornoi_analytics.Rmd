---
title: "Vornoi Play Report"
output: html_document
---



```{r message=FALSE, warning=FALSE, include=FALSE}
quiet <- function(x) {
  sink(tempfile())
  on.exit(sink())
  invisible(force(x))
}
# knitr::opts_knit$set(root.dir = '/home/esteban/roboShutdown')
file_dir="/home/esteban/roboShutdown/output/sf_plots/"
file_dir=paste0(file_dir,gameid,"/")

# 


# quiet({
# g1=get_convex_plot(sf_sample)
# g2=get_triangles_plot(sf_sample)
# g3=get_delaunay_plot(sf_sample)
# g4=get_vornoi_plot(sf_sample)
# g5=get_vornoi_plot_pred(sf_sample)
# })
# g1=get_convex_plot(sf_sample)
# g2=get_triangles_plot(sf_sample)
# g3=get_delaunay_plot(sf_sample)
# g4=get_vornoi_plot(sf_sample)
```
`r sf_sample$playDescription[1]`


```{r , echo=FALSE, out.width = '100%'}
knitr::include_graphics(paste0(file_dir,"area_controlled_epa.png"))
```


```{r , echo=FALSE, out.width = '100%'}
knitr::include_graphics(paste0(file_dir,"area_controlled_team.png"))
```


```{r , echo=FALSE, out.width = '100%'}
knitr::include_graphics(paste0(file_dir,"area_controlled_team_personnel.png"))

# tryCatch({
#   knitr::include_graphics(paste0(file_dir,"VOR-",playid,".png"))
# },error=function(e){
#   print("No Pass")
# })

```

```{r , echo=FALSE, out.width = '100%'}
knitr::include_graphics(paste0(file_dir,"area_controlled_team_personnel.png"))
# knitr::include_graphics(paste0(file_dir,"vornoi-",playid,".mp4"))
```


```{r , echo=FALSE, out.width = '100%'}
knitr::include_graphics(paste0(file_dir,"area_controlled_player_off.png"))
# knitr::include_graphics(paste0(file_dir,"vornoi-",playid,".mp4"))
```

```{r , echo=FALSE, out.width = '100%'}
knitr::include_graphics(paste0(file_dir,"area_controlled_player_def.png"))
# knitr::include_graphics(paste0(file_dir,"vornoi-",playid,".mp4"))
```

```{r , echo=FALSE, out.width = '100%'}
knitr::include_graphics(paste0(file_dir,"area_controlled_player_def_personnel.png"))
# knitr::include_graphics(paste0(file_dir,"vornoi-",playid,".mp4"))
```

```{r , echo=FALSE, out.width = '100%'}
knitr::include_graphics(paste0(file_dir,"area_controlled_player_def_epa.png"))
# knitr::include_graphics(paste0(file_dir,"vornoi-",playid,".mp4"))
```


```{r , echo=FALSE, out.width = '100%'}
knitr::include_graphics(paste0(file_dir,"area_difference_player_def_epa.png"))
# knitr::include_graphics(paste0(file_dir,"vornoi-",playid,".mp4"))
```
